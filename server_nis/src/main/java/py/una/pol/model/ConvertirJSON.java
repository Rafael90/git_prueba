package py.una.pol.model;

import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class ConvertirJSON {
	@SuppressWarnings("unchecked")
	public static String mensajeJSON(String estado, String mensaje) {
		JSONObject obj = new JSONObject();
		obj.put("estado", estado);
		obj.put("mensaje", mensaje);

		return obj.toJSONString();
	}

	@SuppressWarnings("unchecked")
	public static String mensajeDatoJSON(String estado, String mensaje, Nis nis) {
		JSONObject obj = new JSONObject();
		JSONObject objNis = new JSONObject();

		obj.put("estado", estado);
		obj.put("mensaje", mensaje);

		objNis.put("Nis_cliente", nis.getNis_cliente());
		objNis.put("consumo", nis.getConsumo());
		objNis.put("nombre", nis.getNombre());
		objNis.put("apellido", nis.getApellido());
		objNis.put("correo", nis.getCorreo());

		obj.put("nis", objNis);

		return obj.toJSONString();
	}

	@SuppressWarnings("unchecked")
	public static String mensajeDatoJSONUDP(String estado, String mensaje, List<Nis> lista) {
		JSONObject obj = new JSONObject();
		JSONArray listaMensaje = new JSONArray();
		
		obj.put("estado", estado);
		obj.put("mensaje", mensaje);

		for (Nis nis : lista) {
			JSONObject objNis = new JSONObject();
			objNis.put("nis_cliente", nis.getNis_cliente());
			objNis.put("consumo", nis.getConsumo());
			objNis.put("nombre", nis.getNombre());
			objNis.put("apellido", nis.getApellido());
			objNis.put("correo", nis.getCorreo());
			listaMensaje.add(objNis);
		}
		
		obj.put("Nis", listaMensaje);

		return obj.toJSONString();
	}
	
	
}
