package py.una.pol.model;

public class Mensaje {
	
	private String mensaje;
	private String estado;
	
	public Mensaje(String mensaje, String estado) {

		this.mensaje = mensaje;
		this.estado = estado;
	}

	public String getMensaje() {
		return mensaje;
	}
	
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "Mensaje [mensaje=" + mensaje + ", estado=" + estado + "]";
	}
	
}
