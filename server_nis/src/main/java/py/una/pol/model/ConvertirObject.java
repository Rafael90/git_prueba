package py.una.pol.model;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class ConvertirObject {
	public static int obtenerMetodo(String str) throws Exception {
		JSONParser parser = new JSONParser();

		Object obj = parser.parse(str.trim());
		JSONObject jsonObject = (JSONObject) obj;

		int metodo = Integer.parseInt((String) jsonObject.get("metodo"));

		return metodo;
	}

	public static Nis obtenerNis(String str) throws Exception {
		Nis nis = new Nis(0L, 0L, "","","");
		JSONParser parser = new JSONParser();

		Object obj = parser.parse(str.trim());
		JSONObject jsonObject = (JSONObject) obj;

		obj = parser.parse(jsonObject.get("nis_cliente").toString().trim());
		jsonObject = (JSONObject) obj;

		nis.setNis_cliente((Long) jsonObject.get("nis_cliente"));
		nis.setConsumo((Long) jsonObject.get("consumo"));
		nis.setNombre((String) jsonObject.get("nombre"));
		nis.setApellido((String) jsonObject.get("apellido"));
		nis.setCorreo((String) jsonObject.get("correo"));
		
		return nis;
	}

	
	public static String obtenerCorreoSesion(String str) throws Exception {
		JSONParser parser = new JSONParser();

		Object obj = parser.parse(str.trim());
		JSONObject jsonObject = (JSONObject) obj;

		obj = parser.parse(jsonObject.get("sesion").toString().trim());
		jsonObject = (JSONObject) obj;

		return (String) jsonObject.get("correo");
	}

	public static String obtenerCorreo(String str) throws Exception {
		JSONParser parser = new JSONParser();

		Object obj = parser.parse(str.trim());
		JSONObject jsonObject = (JSONObject) obj;

		String correo = (String) jsonObject.get("correo");

		return correo;
	}

	public static Mensaje obtenerMensaje(String str) throws Exception {
		Mensaje mensaje = new Mensaje("", "");
		JSONParser parser = new JSONParser();

		Object obj = parser.parse(str.trim());
		JSONObject jsonObject = (JSONObject) obj;

		obj = parser.parse(jsonObject.get("mensaje").toString().trim());
		jsonObject = (JSONObject) obj;

		mensaje.setMensaje((String) jsonObject.get("mensaje"));
		mensaje.setEstado((String) jsonObject.get("estado"));

		return mensaje;
		
	}

}
