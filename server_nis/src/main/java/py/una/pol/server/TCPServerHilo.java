package py.una.pol.server;

import java.net.*;
import java.util.Date;
import java.io.*;

import py.una.pol.model.ConvertirObject;
//import py.una.pol.model.Mensaje;
import py.una.pol.model.Nis;
//import py.una.pol.service.MensajeService;
import py.una.pol.service.NisService;


import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;






public class TCPServerHilo extends Thread {
	
	
	private final Logger logger = LogManager.getLogger(this.getClass());



	private Socket socket = null;

	public TCPServerHilo(Socket socket) {
		super("TCPServerHilo");
		this.socket = socket;
	}

	public void run() {

		try {
			PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			out.println("Bienvenido!");
			String inputLine, outputLine;
			Integer metodo;

			while ((inputLine = in.readLine()) != null) {
				
				
				System.out.println();
				System.out.println(socket.getLocalSocketAddress()); 
				logger.info("Mensaje recibido: " + inputLine );
				System.out.println("Mensaje recibido: " + inputLine);

				if (inputLine.equals("Bye")) {
					out.println(inputLine);
					break;
				}

				outputLine = "Eco : " + inputLine;

				metodo = ConvertirObject.obtenerMetodo(inputLine);
				
				
				String mensajeImprimir = "la ip es: " + socket.getLocalAddress()  + " el metodoes : " + metodo + String.valueOf( new Date());
				
				
				logger.info(mensajeImprimir);
				System.out.println(mensajeImprimir);
				
				
				// Metodo 1: Registro
				if (metodo == 1) {
					Nis nis = ConvertirObject.obtenerNis(inputLine);
					System.out.println("Entered integer is: "
	                           + nis.getNis_cliente());
					outputLine = NisService.registrarConsumo(nis);
					
				
				// Metodo 2: conectar
				} else if (metodo == 2) {
					String correo = ConvertirObject.obtenerCorreoSesion(inputLine);
					outputLine = NisService.sesionNis(correo);
					
				}

				System.out.println("Servidor: " + outputLine);
				out.println(outputLine);
			}
			
			
			out.close();
			in.close();
			socket.close();
			System.out.println("Finalizando Hilo");

		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
