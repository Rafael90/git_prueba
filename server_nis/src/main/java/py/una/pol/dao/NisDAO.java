package py.una.pol.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import py.una.pol.model.Nis;

public class NisDAO {
	public long insertar(Nis p) throws SQLException {

		String consulta = "INSERT INTO nis(nis_cliente, consumo, nombre, apellido, correo) VALUES(?, ?, ?, ?, ?)";

		long id = 0;
		Connection conn = null;

		try {
			conn = Bd.connect();
			PreparedStatement pstmt = conn.prepareStatement(consulta, Statement.RETURN_GENERATED_KEYS);
			pstmt.setLong(1, p.getNis_cliente());
			pstmt.setLong(2, p.getConsumo());
			pstmt.setString(3, p.getNombre());
			pstmt.setString(4, p.getApellido());
			pstmt.setString(5, p.getCorreo());

			int affectedRows = pstmt.executeUpdate();
			if (affectedRows > 0) {
				try (ResultSet rs = pstmt.getGeneratedKeys()) {
					if (rs.next()) {
						id = rs.getLong(1);
					}
				} catch (SQLException ex) {
					System.out.println(ex.getMessage());
					
				}
			}
		} catch (SQLException ex) {
			
			System.out.println("Error en la insercion: " + ex.getMessage());
			
		} finally {
			try {
				conn.close();
			} catch (Exception ef) {
				System.out.println("No se pudo cerrar la conexion a BD: " + ef.getMessage());
				
			}
		}

		return id;
	}

	public long actualizar(Nis p) throws SQLException {

		String consulta = "UPDATE nis SET consumo=?, nombre = ?, apellido = ?, correo = ? WHERE nis_cliente = ? ";

		long id = 0;
		Connection conn = null;

		try {
			conn = Bd.connect();
			PreparedStatement pstmt = conn.prepareStatement(consulta, Statement.RETURN_GENERATED_KEYS);
			
			pstmt.setLong(1, p.getConsumo());
			pstmt.setString(2, p.getNombre());
			pstmt.setString(3, p.getApellido());
			pstmt.setString(4, p.getCorreo());
			pstmt.setLong(5, p.getNis_cliente());

			int affectedRows = pstmt.executeUpdate();
			if (affectedRows > 0) {
				try (ResultSet rs = pstmt.getGeneratedKeys()) {
					if (rs.next()) {
						id = rs.getLong(1);
					}
				} catch (SQLException ex) {
					System.out.println(ex.getMessage());
				}
			}
		} catch (SQLException ex) {
			System.out.println("Error en la actualizacion: " + ex.getMessage());
		} finally {
			try {
				conn.close();
			} catch (Exception ef) {
				System.out.println("No se pudo cerrar la conexion a BD: " + ef.getMessage());
			}
		}
		return id;
	}

	public long borrar(long nis_cliente) throws SQLException {

		String consulta = "DELETE FROM nis WHERE nis_cliente = ? ";

		long id = 0;
		Connection conn = null;

		try {
			conn = Bd.connect();
			PreparedStatement pstmt = conn.prepareStatement(consulta);
			pstmt.setLong(1, nis_cliente);

			int affectedRows = pstmt.executeUpdate();
			if (affectedRows > 0) {
				// get the ID back
				try (ResultSet rs = pstmt.getGeneratedKeys()) {
					if (rs.next()) {
						id = rs.getLong(1);
					}
				} catch (SQLException ex) {
					System.out.println(ex.getMessage());
				}
			}
		} catch (SQLException ex) {
			System.out.println("Error en la eliminación: " + ex.getMessage());
		} finally {
			try {
				conn.close();
			} catch (Exception ef) {
				System.out.println("No se pudo cerrar la conexion a BD: " + ef.getMessage());
			}
		}

		return id;
	}

	public List<Nis> seleccionarPorNis_cliente(long nis_cliente) {
		String consulta = "SELECT consumo, nombre, apellido, correo FROM nis WHERE nis_cliente = ? ";

		List<Nis> lista = new ArrayList<Nis>();

		Connection conn = null;
		try {
			conn = Bd.connect();
			PreparedStatement pstmt = conn.prepareStatement(consulta);
			pstmt.setLong(1, nis_cliente);

			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				Nis p = new Nis(0L, 0L, "","","");
				p.setNis_cliente(nis_cliente);
				p.setConsumo(rs.getLong(1));
				p.setNombre(rs.getString(2));
				p.setApellido(rs.getString(3));
				p.setCorreo(rs.getString(4));
				
				lista.add(p);
			}

		} catch (SQLException ex) {
			System.out.println("Error en la seleccion: " + ex.getMessage());
		} finally {
			try {
				conn.close();
			} catch (Exception ef) {
				System.out.println("No se pudo cerrar la conexion a BD: " + ef.getMessage());
			}
		}

		return lista;
	}
	
	public List<Nis> seleccionarPorCorreo(String correo) {
		String consulta = "SELECT nis_cliente, consumo, nombre, apellido FROM nis WHERE correo = ? ";

		List<Nis> lista = new ArrayList<Nis>();

		Connection conn = null;
		try {
			conn = Bd.connect();
			PreparedStatement pstmt = conn.prepareStatement(consulta);
			pstmt.setString(1, correo);

			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				Nis p = new Nis(0L, 0L, "", "", "");
				p.setNis_cliente(rs.getLong(1));
				p.setConsumo(rs.getLong(2));
				p.setNombre(rs.getString(3));
				p.setApellido(rs.getString(4));
				p.setCorreo(correo);

				lista.add(p);
			}

		} catch (SQLException ex) {
			System.out.println("Error en la seleccion: " + ex.getMessage());
		} finally {
			try {
				conn.close();
			} catch (Exception ef) {
				System.out.println("No se pudo cerrar la conexion a BD: " + ef.getMessage());
			}
		}

		return lista;
	}
	
}
