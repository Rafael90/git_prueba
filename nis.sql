CREATE TABLE public.nis
(
	nis_cliente integer NOT NULL,
	consumo integer NOT NULL,
	nombre character varying(20),
	apellido character varying(20),
	correo character varying(20) NOT NULL,
	CONSTRAINT pk_cedula PRIMARY KEY (nis_cliente)
);