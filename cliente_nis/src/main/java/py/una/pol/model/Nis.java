package py.una.pol.model;


public class Nis {
	private Long nis_cliente;
	private Long consumo;
	private String nombre;
	private String apellido;
	private String correo;
	
	public Nis(Long nis_cliente, Long consumo, String nombre ,String apellido, String correo ) {
		this.nis_cliente = nis_cliente;
		this.consumo = consumo;
		this.nombre = nombre;
		this.apellido = apellido;
		this.correo = correo;
		
	}

	public Long getNis_cliente() {
		return nis_cliente;
	}

	public void setNis_cliente(Long nis_cliente) {
		this.nis_cliente = nis_cliente;
	}

	public Long getConsumo() {
		return consumo;
	}

	public void setConsumo(Long consumo) {
		this.consumo = consumo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}	
	
	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

}