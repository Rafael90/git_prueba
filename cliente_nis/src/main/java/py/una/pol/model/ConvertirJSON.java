package py.una.pol.model;

import org.json.simple.JSONObject;

public class ConvertirJSON {
	@SuppressWarnings("unchecked")
	public static String generarNisJSON(String metodo, Nis p) {
		JSONObject obj = new JSONObject();
		JSONObject objNis = new JSONObject();
		obj.put("metodo", metodo);
		objNis.put("nis_cliente", p.getNis_cliente());
		objNis.put("consumo", p.getConsumo());
		objNis.put("nombre", p.getNombre());
		objNis.put("apellido", p.getApellido());
		objNis.put("correo", p.getCorreo());
		obj.put("nis_cliente", objNis);

		return obj.toJSONString();
	}
	
	@SuppressWarnings("unchecked")
	public static String generarSesionJSON(String correo) {
		JSONObject obj = new JSONObject();
		JSONObject objSesion = new JSONObject();
		obj.put("metodo", "2");
		objSesion.put("correo", correo);
		obj.put("sesion", objSesion);

		return obj.toJSONString();
	}

	@SuppressWarnings("unchecked")
	public static String generarMensajeJSON(Mensaje m) {
		JSONObject obj = new JSONObject();
		JSONObject objMensaje = new JSONObject();
		obj.put("metodo", "4");
		objMensaje.put("mensaje", m.getMensaje());
		objMensaje.put("estado", m.getEstado());
		obj.put("mensaje", objMensaje);

		return obj.toJSONString();
	}
	

	@SuppressWarnings("unchecked")
	public static String generarCorreoJSONUDP(String correo) {
		JSONObject obj = new JSONObject();
		obj.put("metodo", "4");
		obj.put("correo", correo);

		return obj.toJSONString();
	}
	 
}
