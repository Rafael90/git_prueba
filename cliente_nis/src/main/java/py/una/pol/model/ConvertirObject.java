package py.una.pol.model;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class ConvertirObject {
	public static int obtenerEstado(String str) throws Exception {
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(str.trim());
		JSONObject jsonObject = (JSONObject) obj;
		int estado = Integer.parseInt((String) jsonObject.get("estado"));

		return estado;
	}
	
	public static String obtenerMensaje(String str) throws Exception {
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(str.trim());
		JSONObject jsonObject = (JSONObject) obj;
		String mensaje = (String) jsonObject.get("mensaje");

		return mensaje;
	}

	public static Nis obtenerNis(String str) throws Exception {
		Nis nis = new Nis(0L, 0L, "", "","");
		JSONParser parser = new JSONParser();

		Object obj = parser.parse(str.trim());
		JSONObject jsonObject = (JSONObject) obj;

		obj = parser.parse(jsonObject.get("nis").toString().trim());
		jsonObject = (JSONObject) obj;

		nis.setNis_cliente((Long) jsonObject.get("nis_cliente"));
		nis.setConsumo((Long) jsonObject.get("consumo"));
		nis.setNombre((String) jsonObject.get("nombre"));
		nis.setApellido((String) jsonObject.get("apellido"));
		nis.setCorreo((String) jsonObject.get("correo"));
		return nis;
	}
	
	

	public static List<Mensaje> obtenerListaMensaje(String str) throws Exception {
		List<Mensaje> list = new ArrayList<Mensaje>();
		JSONParser parser = new JSONParser();

		Object obj = parser.parse(str.trim());
		JSONObject jsonObject = (JSONObject) obj;

		JSONArray jsonArray = (JSONArray) jsonObject.get("mensaje");

		for (Object object : jsonArray) {
			JSONObject jsonMensaje = (JSONObject) object;
			String mensaje = (String) jsonMensaje.get("mensaje");
			String estado = (String) jsonMensaje.get("estado");

			list.add(new Mensaje(mensaje, estado));
		}

		return list;
	}
	
	
	public static List<Nis> obtenerListaNis(String str) throws Exception {
		
		System.out.println(str);
		List<Nis> list = new ArrayList<Nis>();
		JSONParser parser = new JSONParser();

		Object obj = parser.parse(str.trim());
		JSONObject jsonObject = (JSONObject) obj;

		JSONArray jsonArray = (JSONArray) jsonObject.get("Nis");

		for (Object object : jsonArray) {
			JSONObject jsonNis = (JSONObject) object;
			Nis nis = new Nis(0L,0L,"","","");
			nis.setNis_cliente((Long) jsonNis.get("nis_cliente"));
			nis.setConsumo((Long) jsonNis.get("consumo"));
			nis.setNombre((String) jsonNis.get("apellido"));
			nis.setApellido((String) jsonNis.get("apellido"));
			nis.setCorreo((String) jsonNis.get("correo"));
			list.add(nis);
		}

		return list;
	}
}
