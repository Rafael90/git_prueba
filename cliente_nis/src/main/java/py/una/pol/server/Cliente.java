package py.una.pol.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;

import py.una.pol.model.ConvertirJSON;
import py.una.pol.model.ConvertirObject;
import py.una.pol.model.Nis;
import py.una.pol.template.Ventana;
import java.util.ArrayList;

public class Cliente {
	public static void main(String[] args) throws Exception {
		Socket unSocket = null;
		PrintWriter out = null;
		BufferedReader in = null;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String opcion = "";
		String mensajeClient = "";
		String mensajeServer = "";
		int estado;
		Nis nisActiva = new Nis(0L,0L,"","","");
		
		
		try {
			unSocket = new Socket("localhost", 4444);
			// enviamos nosotros
			out = new PrintWriter(unSocket.getOutputStream(), true);
			// viene del servidor
			in = new BufferedReader(new InputStreamReader(unSocket.getInputStream()));
		} catch (UnknownHostException e) {
			System.err.println("Host desconocido");
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Error de I/O en la conexion al host");
			System.exit(1);
		}

		System.out.println("Servidor: " + in.readLine());
		
		estado = -1;
		Ventana.ventanaPrincipal();
		opcion = br.readLine();
		
		while (opcion.trim().compareTo("4") != 0) {
			
			// Opcion 1: Registrar consumo
			if (opcion.trim().compareTo("1") == 0) {
				while (estado != 0) {
				//Ventana.ventanaRegistro();
				System.out.print("Nis_cliente: ");
				Long nis_cliente = Long.parseLong(br.readLine());
				System.out.print("Consumo: ");
				Long consumo = Long.parseLong(br.readLine());
				
				System.out.print("Nombre: ");
				String nombre = br.readLine();
				System.out.print("Apellido: ");
				String apellido = br.readLine();
				System.out.print("Correo: ");
				String correo = br.readLine();
				
				Nis nis = new Nis(nis_cliente, consumo, nombre,apellido,correo);
				
				mensajeClient = ConvertirJSON.generarNisJSON("1", nis);

				System.out.println("Cliente: " + mensajeClient);
				out.println(mensajeClient);
				mensajeServer = in.readLine();
				System.out.println("Servidor: " + mensajeServer);
				
				//si estado es cero se registro consumo con exito y sale del while
				estado = ConvertirObject.obtenerEstado(mensajeServer);
				Ventana.ventanaMensajeRespuesta(ConvertirObject.obtenerMensaje(mensajeServer));
				
			}
		  } 
			//opcion 2: conectar
			else if ( opcion.trim().compareTo("2") == 0) {
				String correo = "";
				opcion = "";
				while (estado != 0) {
					Ventana.ventanaSesion();
					System.out.print("Correo: ");
					correo = br.readLine();

					mensajeClient = ConvertirJSON.generarSesionJSON(correo);

					//System.out.println("Cliente: " + mensajeClient);
					out.println(mensajeClient);
					mensajeServer = in.readLine();
					
					System.out.println("Servidor: " + mensajeServer);
					Ventana.ventanaMensajeRespuesta(ConvertirObject.obtenerMensaje(mensajeServer));
					
					estado = ConvertirObject.obtenerEstado(mensajeServer);
					
					if (estado == 0) {
						nisActiva = ConvertirObject.obtenerNis(mensajeServer);
						iniciarServerUDP(nisActiva);
					} 
			
			 }
			}
			//opcion 3: Listar conectados
			else if ( opcion.trim().compareTo("3") == 0) {
				nisDisponibles(nisActiva);
			
		  }
			//opcion 4: Desconectarse
			else if(opcion.trim().compareTo("4") == 0 ) {
				  System.out.println("Bye");  
			  }
			
			
			Ventana.ventanaPrincipal();
			opcion= "";
			opcion = br.readLine();
			
		}  
		cerrarServerUDP(nisActiva);
		out.println("Bye");
		out.close();
		in.close();
		unSocket.close();
		
	}
	
	public static void nisDisponibles(Nis nisActual) throws Exception {
		try {
            DatagramSocket clientSocket = new DatagramSocket();
            int puertoServidor = 9876;
            InetAddress IPAddress = InetAddress.getByName("127.0.0.1");
            //System.out.println("Intentando conectar a = " + IPAddress + ":" + puertoServidor +  " via UDP...");

            byte[] sendData = new byte[1024];
            byte[] receiveData = new byte[1024];
            String respuesta = "";            
        
            String datoPaquete = ConvertirJSON.generarNisJSON("3", nisActual);
            sendData = datoPaquete.getBytes();
            //System.out.println("Cliente: " + datoPaquete);
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, puertoServidor);
            clientSocket.send(sendPacket);
                 
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            clientSocket.receive(receivePacket);
            respuesta = new String(receivePacket.getData());            
            System.out.println("Servidor: " + respuesta);
            
            int estado = ConvertirObject.obtenerEstado(respuesta);
            
            if (estado == 0) {
            	List<Nis> nisLista = ConvertirObject.obtenerListaNis(respuesta);
            	Ventana.ventanaListaConectado(nisLista, nisActual.getCorreo()); 
            } else {
            	System.out.println("Error al obtener la lista de conectados");
            }
            clientSocket.close();
		} catch (UnknownHostException ex) {
            System.err.println(ex);
        } catch (IOException ex) {
            System.err.println(ex);
        }	
	}
	


	public static void iniciarServerUDP(Nis nisActual) throws Exception {
		try {
            DatagramSocket clientSocket = new DatagramSocket();
            int puertoServidor = 9876;
            InetAddress IPAddress = InetAddress.getByName("127.0.0.1");
            //System.out.println("Intentando conectar a = " + IPAddress + ":" + puertoServidor +  " via UDP...");

            byte[] sendData = new byte[1024];            
            String datoPaquete = ConvertirJSON.generarNisJSON("2", nisActual);
            sendData = datoPaquete.getBytes();
            //System.out.println("Cliente: " + datoPaquete);
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, puertoServidor);
            clientSocket.send(sendPacket);
                 
            clientSocket.close();
		} catch (UnknownHostException ex) {
            System.err.println(ex);
        } catch (IOException ex) {
            System.err.println(ex);
        }	
	}
	

	
	public static void cerrarServerUDP(Nis nisActual) throws Exception {
		try {
            DatagramSocket clientSocket = new DatagramSocket();
            int puertoServidor = 9876;
            InetAddress IPAddress = InetAddress.getByName("127.0.0.1");
            
            byte[] receiveData = new byte[1024];
            String respuesta = "";

            byte[] sendData = new byte[1024];
            String datoPaquete = ConvertirJSON.generarCorreoJSONUDP(nisActual.getCorreo());
            sendData = datoPaquete.getBytes();
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, puertoServidor);
            clientSocket.send(sendPacket);
            
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            clientSocket.receive(receivePacket);
            respuesta = new String(receivePacket.getData());            
            System.out.println("Servidor: " + respuesta);
            
            
            
            clientSocket.close();
		} catch (UnknownHostException ex) {
            System.err.println(ex);
        } catch (IOException ex) {
            System.err.println(ex);
        }	
	}
}	